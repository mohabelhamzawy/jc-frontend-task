import { LocaleService } from './../../services/locale.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  selectedCountryId: any;
  countryFlag: string;
  countries: any[];

  constructor(private  _localeService:LocaleService) {
    this.countries = this._localeService.getCountries();
    this.selectedCountryId = localStorage.getItem('country');
  }

  ngOnInit(): void {
    this.setInitCountry();
    this.getCountryFlag();
  }

  // Set Initial Country Function
  setInitCountry() {
    if ( !localStorage.country ) {
      localStorage.setItem('country', this._localeService.getCountries()[0].country_id);
    }
  }

  // Change Country Function
  changeCountry(event) {
    localStorage.setItem('country', event.target.value);

    this._localeService.isCountryChanged.next(true);
  }

  // Set Country Flag Function
  getCountryFlag() {
    let currentCountryId = localStorage.getItem('country');
    let currentCountryIcon = this._localeService.localeCountries.find(item => item.country_id == currentCountryId);

    console.log(currentCountryIcon.iconName);
    this.countryFlag = currentCountryIcon.iconName;
  }
}