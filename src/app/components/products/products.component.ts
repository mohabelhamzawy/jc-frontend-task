import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { LocaleService } from 'src/app/services/locale.service';
import { ProductsService } from 'src/app/services/products.service';
import { Item } from 'src/app/interfaces/items';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {

  productsCollection: Item[] = [];
  currency: string;
  countryChangedSubscription: Subscription = new Subscription();
  ProductsSubscription: Subscription = new Subscription();


  constructor(private _products: ProductsService, private _localeService:LocaleService) { }

  ngOnInit(): void {
    this.setDecimal();
    
    // Get Products on change
    this.countryChangedSubscription = this._localeService.isCountryChanged.subscribe(data => {
      this.productsCollection = [];
      this.printProducts();
    });
    
  }

  // Print Products
  printProducts() {
    this.ProductsSubscription = this._products.getProducts().subscribe(data => {

      setTimeout(() => {
        this.productsCollection = this._products.filterProducts(data, localStorage.country);
      }, 1500);
    })
  }


  // Set currency
  setCurrency() {
    let myCountry = this._localeService.getCountries().find(item => item.country_id == localStorage.getItem('country'));
    return myCountry.currency;
  }

  // Set decimal
  setDecimal(): string {
    let myCountryId = localStorage.getItem('country');
    return (myCountryId == '1' || myCountryId == '2') ? '1.3-3' : '1.2-2'; 
  }

  ngOnDestroy(): void {
    this.ProductsSubscription.unsubscribe();
    this.countryChangedSubscription.unsubscribe();
  }
}