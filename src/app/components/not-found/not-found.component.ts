import { Component, OnInit } from '@angular/core';
import { AnimationOptions } from 'ngx-lottie';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  options: AnimationOptions = {
    path: 'assets/not-found.json'
  }

  constructor() { }

  ngOnInit(): void {
  }

}
