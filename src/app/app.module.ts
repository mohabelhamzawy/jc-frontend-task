import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modules
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';

// Components
import { HeaderComponent } from './components/header/header.component';
import { ProductsComponent } from './components/products/products.component';

// Services
import { ProductsService } from './services/products.service';
import { LocaleService } from 'src/app/services/locale.service';

// Interceptors
import { interceptors } from './interceptors/interceptors';
import { NotFoundComponent } from './components/not-found/not-found.component'; 

export function playerFactory() {
  return player
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProductsComponent,
    NotFoundComponent
  ],
  imports: [
    LottieModule.forRoot({player: playerFactory}),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ProductsService, LocaleService, interceptors],
  bootstrap: [AppComponent]
})
export class AppModule { }
