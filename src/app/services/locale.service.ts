import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocaleService {

  localeCountries: any[];
  isCountryChanged: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor() {
    this.localeCountries = [
      {
        name: 'Kuwait',
        country_id: 1,
        currency: 'KWD',
        iconName: 'kuwait'
      },
      {
        name: 'Bahrain',
        country_id: 2,
        currency: 'BHD',
        iconName: 'bahrain'
      },
      {
        name: 'UAE',
        country_id: 3,
        currency: 'AED',
        iconName: 'united-arab-emirates'
      },
      {
        name: 'Qatar',
        country_id: 4,
        currency: 'QAR',
        iconName: 'qatar'
      },
      {
        name: 'Saudi Arabia',
        country_id: 5,
        currency: 'SAR',
        iconName: 'saudi-arabia'
      }
    ];

  }

  getCountries() {
    return this.localeCountries;
  }
}