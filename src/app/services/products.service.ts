import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Item } from '../interfaces/items';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  url: string;

  constructor(private http: HttpClient) {
    this.url = environment.itemsUrl
  }

  // Get All Products
  getProducts(): Observable<Item> {
    return this.http.get<Item>(this.url);
  }

  // Products filteration
  filterProducts(obj, countryId: number): Item[] {
    return obj.filter(item => item.country_id == countryId);
  }
}