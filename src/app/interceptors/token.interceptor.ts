import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const token = {app_token: localStorage.getItem('app_token')};
    const hasToken = localStorage.app_token;

    // Token checker 
    if ( hasToken ) {
      request = request.clone({
        setHeaders: token
      });
    }

    return next.handle(request);
  }
}
